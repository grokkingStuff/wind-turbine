%% Purpose of this script


%% Including units
u = symunit;


%% Standard Air Proprties
% Density of air
air_density = 1.225*(u.kg/u.m^3); %[kg/m^3]


%% Geometric Properties
% Diameter of Turbine
D = sym('D')*u.m;  %[m]
% Swept Area
A = 0.25*pi*D^2;
% Number of Blades
n = 3;



%% Induction Factors
% axial momntum induction factor
a = sym('a');
% angular momntum induction factor
a_prime = sym('a_prime');

%% Velocity
% Velocity far upstream
v_upstream = sym('v_inf')*(u.m/u.s);
% Velocity at rotor
v_rotor = (1-a)*v_upstream;
% Velocity far downstream
v_downstream = (1-2*a)*v_upstream;


% Lift coefficient
c_l = sym('c_l');
c_d = sym('c_d');
phi = sym('phi');

% Rotating the lift & drag coefficients about phi
c_x = c_l*cos(phi) + c_d*sin(phi);
c_y = c_l*sin(phi) - c_d*cos(phi);



%% Derived Quantities

F = 0.5*air_density*(v_upstream^2 - v_downstream^2);
P_ext = 2*a*(1-a)*(v_upstream^3)*air_density*A;

simplify(F)
simplify(P_ext)



