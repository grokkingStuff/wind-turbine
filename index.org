#+TITLE: Index







* Project Metainformation
** Team members

| First Name | Last Name | Email Address | HWU ID |
| Kenan      | Jabbour   |               |        |
| Vishakh    | Kumar     |               |        |
| Ahsen      | Majeed    |               |        |
| Shane      | Menzenes  |               |        |
| Hamad      | Najafi    |               |        |
| Leithner   | Sagurit   |               |        |

* Integrating into the electrical grid

** Forecasting timeframes
The forecasting timeframe for the electricity market is as follows:

- 6 months - 1 year:
 Long term outlook such as weather patterns (e.g. droughts), planned major plant or transmission outages.
- 1 week – 6 months:
 Planned outages etc.
- 24 hours - 1 week:
 Short outage planning.
- 12 hours - 24 hours:
 Market scheduling.
- 6 hours - 12 hours:
 Slow start plant scheduling.
- 2 hours - 6 hours:
 Intermediate start plant.
- 2 hours:
 Gate closure and firm offers.


** Strengthening the local distribution

Several  observations  emerge  from  this  case  study  that  may  have  relevance  for  other  clean  energy  technology development and diffusion efforts:

- Government,  industry  and  international  collaborative  RD&D,  spurred  on  by  the  energy  crises  of  the  1970s  in  some  cases,  resulted  in  major  design  improvements  and  increased  technical  and  economic  performance of wind turbines and related components. Subsequently, the successful and rapid diffusion of these technologies, in some countries, by a variety of incentives has revealed a new set of technical and  operational  challenges,  namely  those  of  grid  interconnection.  Since  research  programmes  did  not  keep  pace  with  the  rapid  deployment  of  wind  power,  this  suggests  that  such  efforts  were  either  disconnected from deployment programmes or were not sufficiently well connected.

- Programmes and policies that aim to assist in building new markets and transforming existing markets must  engage  all  stakeholders  along  the  commercialisation  chain  in  an  integrated  and  pre-emptive  fashion.  Policy designers must understand the interests of those involved in the market and there must be effective communication with those stakeholders and the research community.

- The  incentives  for  stakeholders  to  collaborate  include  the  need  to  “learn”  from  technical  and  operational solutions and failed approaches of others, to improve the reliability of tools such as models for  wind  farm  dynamics  and  grid  operation,  to  develop  standardised  approaches  across  market  areas,  and to provide technical expertise for regulatory and standards setting processes.

- RD&D  and  market  deployment  programmes  must  take  account  of  “learning  effects”;  i.e.,  go  beyond  the immediate needs of stakeholders and foresee technical requirements in anticipation of deployment, particularly when new infrastructure is required such as offshore wind farms.

- Responding to a challenge, such as the need to rapidly find technical and operational solutions to grid interconnection  issues,  can  foster  more  co-operative  research  at  national,  regional  and  international  levels.    The  benefits  of  international  collaboration  for  emerging  low-carbon  energy  technologies  include:  pooled  resources,  shared  costs,  harmonisation  of  standards,  and  strengthened  national  R&D  capabilities.

- As an energy technology advances on the path from RD&D to commercialisation, costs and risks shift from  government  to  industry.  As  illustrated  by  the  collaborative  groups  profiled  in  this  study,  grid  integration  research  includes  participation  and  financial  support  from  wind  and  electricity  supply  industries to a significant degree.



** Challenges: Integration into Electricity Systems

Electricity systems must supply power in close balance to demand. The average load varies in predictable daily  and  seasonal  patterns,  but  there  is  an  unpredictable  component  due  to  random  load  variations  and  unforeseen events. To compensate for these variations, additional generation capacity is needed to provide regulation  or  set  aside  as  reserves.  Generators  within  an  electrical  system  have  varying  operating  characteristics: some are base-load plants; others, such as hydro or combustion turbines, are more agile in terms of response to fluctuations and start-up times. There is an economic value above the energy produced to  a  generator  that  can  provide  these  ancillary  services.  Introducing  wind  generation  can  increase  the  regulation  burden  and  need  for  reserves,  due  to  its  natural  intermittency.  The  impact  of  the  wind  plant  variability may range from negligible to significant depending on the level of penetration and intermittency of the wind resource.

** Issues

Unfortunately the turbines at West Wind produced tonal noise.

The problem was identified as a new model gearbox. The turbines produced two tones; one at low speed and one at high speed. A successful solution was developed involving both software and hardware. During the development of the wind industry a number of lessons have been learned and shared at the wind energy conference and workshops.

Key lessons include:
•Wind turbines can, unexpectedly, create noise.
•Systems need to be in place to enable communities to easily report noise issues.
•The owner of the windfarm needs to engage with the community and respond to complaints about wind farms.
•A system for assessing and managing windfarm noise needs to be developed that has wide buy-in from noise experts.A difficulty with windfarm noise is that once a community becomes sensitive to wind farm noise the issue can escalate.




* Increase in wind turbine sizes

Enhanced performance and cost reductions have been closely related to increases in turbine size. Until the mid-1980s,  turbines  were  typically  less  than  100  kW  with  rotor  diameters  of  about  20  metres.  This  increased to a few hundred kilowatts by the mid-1990s, when turbine sizes began to range from 0.5 – 1.5 MW  (Figure  2).  New  turbines  deployed  in  2002  averaged  about  1  170  kW.  In  2002,  a  large  offshore  demonstration  project  in  Denmark  was  commissioned  using  2  –  2.3  MW  turbines,  while  in  Germany,  average  wind  turbine  size  reached  1.4  MW.  The  largest  turbines  being  manufactured  today  are  4.5  MW  capacity with a rotor diameter of 112 metres. A prototype 5 MW turbine with a 124 metres rotor diameter, the largest in the world, has been installed recently in Germany.

* Wind turbines as a means to bolster the electrical grid in the last mile
Renewable energy sources have the main advantage of power generation as stand-alone or grid connected and suitable for the remote areas.  Therefore, it can also help support the energy security in the last mile of the grid connection.

* Economics of wind energy by Rom Cronin

- Four phases of typical wind farm timeline
- Five major components of cost
- Economic profile

** Four phases of typical wind farm timeline
  - Development (~5 to 10 years)
    - Finding a site
    - Site characteristics
    - Wind farm design
    - Permissions & Licenses
    - Financing
    - Public Engagement
      (Involve the public as early as possible and throughout the lifecycle)
  - Implementation (~1 to 2 years)
    Capital Intensive
    - Procurement
    - Construction

  - Operation
    Maintenance

  - Decommision
    Depends on how well the turbines have been maintained.
    Either remove the turbine or replace it with a better one


** Five major components of cost

- Development
  Site assessment
  Wind resources
  Permissions and licenses
  Preliminary design
  Legal & contracts
  Environment study

  50MW onshore 65M Euro

- Implementation
  Turbines
  Foundations
  Sub-station
  Cabling
  Grid connection

- Operation
  Maintenance
  Administration
  Operation

  40k Euro per turbine


- Decommision


** Economic profile

Wind farm economic profile

#+BEGIN_QUOTE
Wind turbines are expensive generating installation investments. Hence the production costs are highly dependent on installation costs and their financing.

Offshore Wind Power - Danish Experiences and Solutions
Published by the Danish Energy Agency
October 2005
#+END_QUOTE

Wind energy projects are capital intensive compared to conventional energy projects.

* Project Construction

** Roads and Quarries

The 34-foot wide road bed was designed and constructed to accommodate the width and weight of a 31-foot wide crane and includeda 16-foot wide compacted crushed rock surface for vehicle travel.Because of the size of the cranes, the road design also limited vertical grade, vertical curves and side slope of the travel surface.To allow erection ofthe towers,a crane pad was constructed near each tower site using small dozers.  The soil was primarily hard fractured basalt that required ripping,but there were alsoareas of soft compressible silt that required use ofgeo-textile fabric.  Two on-sitequarries were centrally located.  Portable crushers were used to produce1 ¼ inch minus and ¾ inch minus basalt aggregate, which was later used both to make concrete at the mobile concrete batch plant,and as a final surfacing for the roads.  With the exception of a portion of the concrete sand, all of the aggregates for the project were provided by the on-site quarries.

** Foundations

The towers aresupported by a cast-in-place,post-tensioned,two-foot thick concrete “ring” ranging from 25 to 32 feet in length.  Adrill core sample was taken at the center of each foundation locationto aid in the preparation ofa geotechnical report.  Each tower foundation was located in the field using Global Positioning System (GPS) technology.Since drilling and blasting was required for most foundation locations, a drilling crew drilled the rock to a depth of two feet beyond the design depthof the foundations. Drilling proceeded at the rate of two to three foundations per day, followed by blasting of four to five foundations per day.  An excavating crew followed the blasting crew using anextended-boomexcavatorranging in size from a Caterpillar (CAT)320 to a CAT 365.Foundation excavation continuedat the rate of one and one-halfto twofoundation holes per day.
Each tower foundationconsists of a post-tensioned high-strength concrete ring formed by two Corrugated Metal Pipe (CMP)forms.  The outside CMP is 14 feet in diameter and the inside CMP is ten feet in diameter.After excavation,a rough-terrain crane was used to lift and set the outside CMP in the hole.  The CMPwas then stabilized using concrete gravity blocksandcables and then backfilled on the outside with a 300 psi compressive strengthcement-based slurry.  Post-tensioning for each concrete foundation “ring” wasprovided by a “bolt cage” consisting of 120 high-strength bolts.  Because the bolts serve as the post-tensioning mechanism, all but the ends of the bolts wereencased in a greased sleeve.  The cage was assembled in an upright position by a crew who first fitted the tops of the bolts into a steel ring template that matchedthe bolt pattern in the base of the towers.  The boltcage was then liftedfrom the foundation hole by a crane and the bottom steel embedment ring was permanently attached,with a nut on each rod beneath the ring.  Next,the inner CMPwas lowered into the foundation hole, followed by the bolt cage, as shown in Figure 2.Thisassembly was then centered between the two CMPsand cribbed to the proper elevation.



* Design Planning Report

Design Specification - All design variables have beem identified

Optimization - It is clear what theoretical analysis will be employed to help optimize all design variables

Planning - The project has been planned to equally attribute effort, to invite engagement from all group members and to do so in a timeline manner


* Project Planning

Should focus more on the WBS than anything else. Talk about the methodology for each before proceeding further

Why offshore wind turbines?

- Stronger and steadier winds
- Less interference with human population


* Design Planning Report


#+LaTeX_HEADER: \documentclass[paper=a4wide, fontsize=12pt]{scrartcl}	 % A4 paper and 11pt font size
#+LaTeX_HEADER: \usepackage[svgnames]{xcolor} % Using colors
#+LaTeX_HEADER: \usepackage{background} % To include background images
#+LaTeX_HEADER: \usepackage{fancyhdr} % Needed to define custom headers/footers
#+LaTeX_HEADER: \usepackage[ a4paper, left=20mm, right=20mm, top=20mm, bottom=2.5cm,  footskip=1.2cm]{geometry}  % Changing size of document
#+LaTeX_HEADER: %\usepackage[square, numbers, comma, sort&compress]{natbib} % Use the natbib reference package - read up on this to edit the reference style; if you want text (e.g. Smith et al., 2012) for the in-text references (instead of numbers), remove 'numbers'

#+LaTeX_HEADER: \usepackage{pgfgantt}
#+LaTeX_HEADER: \usepackage{braket}



%%%%%% Setting up the stye

\setlength\parindent{0pt} % Gets rid of all indentation
\backgroundsetup{contents={\includegraphics[width=\textwidth]{logo.jpg}},scale=1,placement=top,opacity=0.4,position={7.2cm,2cm}} %  OIST Logo

\pagestyle{fancy} % Enables the custom headers/footers

\lhead{} \rhead{} % Headers - all  empty

\title{\vspace{-1.8cm}  \color{DarkBlue} Wind Turbine Proposal}
\subtitle{Optimal design of wind turbine \& verification % Title of the rotation project
\vspace{-2cm} }
\date{} % No date

\lfoot{\color{Grey} Fluids Mechanics, B59EI 2020-2021}
\rfoot{ \color{Grey} Heriot-Watt University Dubai}
\cfoot{\color{Grey} \thepage}

\renewcommand{\headrulewidth}{0.0pt} % No header rule
\renewcommand{\footrulewidth}{0.4pt} % Thin footer rule


* Introduction

Some introduction to the topic. Orgmode is cool, see http://orgmode.org fore details. URLs are converted into hyperrefs automatically (i.e. no need to use the \verb|\href{}{}| command).

* Method
** This document
\label{sec:method}

The overall workflow is this: the main part of the paper is in the
file ~content.org~.

This is translated by an (Emacs) script into a full LaTeX document with
name ~_content.tex~.

An extra (python) script strips off the LaTeX preamble and the
last few lines at the end of the document, and saves the
remaining middle part of the document as ~content.tex~.

The main latex file is ~paper.tex~, which contains the latex
preamble, all the style the requirements we or the journal may have,
the abstract, the bibliographystyle and bibliography uses the
\verb|\input{content.tex}| command to include the autogenerated latex
into the main document.

There is a Makefile that automises all of these steps. It may be useful to run a pdf viewer that watches and automatically updates ~paper.pdf~ on any change, and to have a loop (or the ~watch~ command that repeatedly calls ~make~, so that the pdf is re-created when the modified ~content.org~ file is saved).

** Subsections

Our document may well contain subsections. In fact, part of the joy of orgmode is that we can re-arrange sections and change their depth quite easily.

** Mention how to ignore a heading              :ignoreheading:

Using the ~:ignoreheading:~ tag, we can use section headings which do
not appear in the final document.

** Say why this is useful                       :ignoreheading:

This is useful to 'label' paragraphs or sections to draft a document
while not wanting to reveal that label/title in the final version to the
reader.


** Details

We can use equations as if the orgmode source was \LaTeX{}, for example $f(x) = x^2$ or
\begin{equation}
\int f(x) \d x = C
\end{equation}

** Italics, bold, underline

Text can be emphasised using /italics/, *bold* or _underline_.
In org-mode these are written as ~/italics/~, ~*bold*~ and ~_underline_~.

** COMMENT

Note also the comment feature: sections that have titles starting with COMMENT are not included in the output, and can be used to record thoughts or drafts not to be shown in the LaTeX document.

* Results
** Tables

Tables are converted into tables:

| name     |  # |
|----------+----|
| Apple    |  4 |
| Pears    |  5 |
| Tomatoes | 16 |


** Figures

Figures can be included like this

file:figures/icon.png

Or we use the latex command directly:

\includegraphics[width=2cm]{figures/icon.png}


We can also provide guidance to orgmode how we would like the figure included (see http://orgmode.org/manual/LaTeX-specific-attributes.html for details):

#+ATTR_LATEX: :width 2cm :options angle=90
file:figures/icon.png

If you prefer, you can inculde the full float figure environment as
raw latex into the orgmode file. Here is an example for such a figure,
and then we can refer to it through its figure number
\ref{fig:myfigure}.

\begin{figure}
\centering
\includegraphics[width=0.1\columnwidth]{figures/icon.png}
\caption{The skyline\label{fig:myfigure}}
\end{figure}

** Figure and Table environments

The ~+CAPTION~ directive instructions orgmode to create a caption, and wrap up the following graphic or table into  a Table or Figure environment.

#+CAPTION: This is the caption for the next table (or link)
#+NAME:   tab:basic-data
| Apples  | 1 |
| Bananas | 3 |
|---------+---|
| Fruit   | 4 |

#+CAPTION[Short form]: This is the caption for the next Figure
#+NAME:   fig:example
#+ATTR_LATEX: :width 2cm :options angle=90
file:figures/icon.png

We have just created figure \ref{fig:example}.


** Code

We can also include code. Again, it can be done directly from orgmode (and the option to also execute the code from within orgmode and include the output is exciting, but goes beyond the purpose of this template for document writing in orgmode. If you want to explore this further, look here: http://orgmode.org/manual/Working-with-source-code.html#Working-with-source-code

\begin{figure}
\footnotesize
\inputminted[bgcolor=white,frame=lines]{python}{code/example1.py}
\normalsize
\caption{An example script in Python. \label{fig:code-example1}}
\end{figure}

Figure \ref{fig:code-example1} shows some code.

** Include literal LaTeX

#+LATEX: If necessary, we can use the \verb|#+LATEX:| directive, to send a string directly to LaTeX, i.e. unmodified by orgmode.

#+BEGIN_EXPORT latex
We can also create a literal \LaTeX{} block like this one.\footnote{See \href{http://orgmode.org/manual/Quoting-LaTeX-code.html}{http://orgmode.org/manual/Quoting-LaTeX-code.html} for more details}.
#+END_EXPORT

** More results

Of course we can cite work \cite{authorX2016}.

* Discussion
Coming back to the method outlined in section \ref{sec:method}, it may
well be possible to achieve a similar setup without the Python script
extracting the main part of the autogenerated document etc, and
include all the required latex setup, extra packages, into special #+
commands in the orgmode file. However, I have found it efficient to be
able to use journal latex templates directly, and thus came up with
this arrangement. Not perfect, but a functional start.

* Summary

I like using orgmode to author documents as the orgmode mark up is less intrusive (and overall fewer characters to type!) than the \LaTeX{} mark up. Rearranging sections, and changing the depth (i.e. move sections to subsections etc) are trivial in orgmode. Overall, orgmode allows me to focus more on the content of the document and its structure.

* Acknowledgements
Thanks to Sam Sinayoko for introducing me to his way of creating
beamer latex slides from orgmode, and who wrote the original elisp
script that executes the conversion of orgmode files to LaTeX. Thanks
also to Maximilian Albert, who helped tidying up the Makefile.
\newpage
* TODO
Sometimes, a section with things to do is useful; with the understanding that this is completed and removed before the document is finished. (Or changed into a COMMENT section, so that it doesn't export to latex.)

An orgmode todo list (which can be nested) looks like this

- TODO [2/3]
  - [X] create github repository
  - [X] write up the setup for this document
  - [ ] Save planet [0/3]
    - [ ] understand challenge
    - [ ] find solution
    - [ ] implement it
