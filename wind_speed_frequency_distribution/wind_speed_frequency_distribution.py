import numpy as np
from scipy.stats import weibull_min
from matplotlib import pyplot as plt


# Define the distribution parameters to be plotted
k_values = [0.5, 1, 2, 2]
lam_values = [1, 1, 1, 2]
linestyles = ['-', '--', ':', '-.', '--']
mu = 0
x = np.linspace(0, 25, 1000)


#------------------------------------------------------------
# plot the distributions
fig, ax = plt.subplots(figsize=(5, 3.75))

for (k, lam, ls) in zip(k_values, lam_values, linestyles):
    dist = weibull_min(k, mu, lam)
    plt.plot(x, dist.pdf(x), ls=ls, c='black',
             label=r'$k=%.1f,\ \lambda=%i$' % (k, lam))

plt.xlim(0, 5)
plt.ylim(0, 1)

#ax.set_xlim([0,25])
#ax.set_title('Long-term hub-height wind speed frequency distribution')
#ax.set_xlabel('Wind speed [m/s]')
#ax.set_ylabel('Frequency')

plt.xlabel('$x$')
plt.ylabel(r'$p(x|k,\lambda)$')
plt.title('Weibull Distribution')

plt.legend()
plt.show()




#fig = plt.figure(figsize=[9,6])
#ax = fig.add_subplot(111)
#freq_by_ws.plot(kind='bar', color=edf_green, width=0.9, ax=ax)
#ax.plot(x, sp.stats.exponweib(1, k, scale=A, loc=0).pdf(x), color=edf_blue)
#ax.set_xlim([0,25])
#ax.set_title('Long-term hub-height wind speed frequency distribution')
#ax.set_xlabel('Wind speed [m/s]')
#ax.set_ylabel('Frequency')
#plt.xticks(rotation=0)
#plt.tight_layout()
#plt.show()