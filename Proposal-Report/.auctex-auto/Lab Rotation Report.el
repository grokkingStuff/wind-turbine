(TeX-add-style-hook
 "Lab Rotation Report"
 (lambda ()
   (TeX-add-to-alist 'LaTeX-provided-class-options
                     '(("scrartcl" "paper=a4wide" "fontsize=12pt")))
   (TeX-add-to-alist 'LaTeX-provided-package-options
                     '(("xcolor" "svgnames") ("geometry" "a4paper" "left=20mm" "right=20mm" "top=20mm" "bottom=2.5cm" "footskip=1.2cm")))
   (TeX-run-style-hooks
    "latex2e"
    "scrartcl"
    "scrartcl10"
    "xcolor"
    "background"
    "fancyhdr"
    "geometry"
    "pgfgantt"
    "braket")
   (LaTeX-add-labels
    "fig:avoid"
    "fig:fid")
   (LaTeX-add-bibitems
    "CRAB"
    "RING"
    "CROSSING"
    "SIMPLEX"
    "OC"))
 :latex)

